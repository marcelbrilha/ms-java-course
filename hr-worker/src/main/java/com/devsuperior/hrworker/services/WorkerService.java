package com.devsuperior.hrworker.services;

import java.util.List;

import com.devsuperior.hrworker.entities.Worker;

public interface WorkerService {
	List<Worker> findAll();

	Worker findById(Long id);
}
