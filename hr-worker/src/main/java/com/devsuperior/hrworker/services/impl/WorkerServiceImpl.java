package com.devsuperior.hrworker.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devsuperior.hrworker.entities.Worker;
import com.devsuperior.hrworker.repositories.WorkerRepository;
import com.devsuperior.hrworker.services.WorkerService;

@Service
public class WorkerServiceImpl implements WorkerService {

	@Autowired
	private WorkerRepository workerRepository;

	@Override
	public List<Worker> findAll() {
		return this.workerRepository.findAll();
	}

	@Override
	public Worker findById(Long id) {
		return this.workerRepository.findById(id).get();
	}

}
